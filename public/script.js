function getSelAns(i) {
    var result = {
        select: '',
        valid: false
    };
    document.querySelectorAll('[name="q' + i + '"]').forEach(function(item) {
        if (item.checked) {
            result.select = item.value;
            result.valid = (item.getAttribute('data-ans-valid') !== null);
        }
    });
    return result;
}

function getResult(quest_count) {
    var t_html = '';

    for(var i = 1; i <= quest_count; i++) {
        var rr = getSelAns(i);

        function vvv() {
            if (rr.select) {
                if (rr.valid) {
                    return '<span style="color: green">Правильно</span>';
                } else {
                    return '<span style="color: red">Неправильно</span>';
                }
            } else {
                return 'Не отвечено';
            }
        }

        t_html += '<tr><td width="33.33%" style="text-align: center">' + i + '</td><td width="33.33%" style="text-align: center">' + rr.select + '</td><td width="33.33%">' + vvv() + '</td></tr>';
    }

    results.innerHTML = t_html;

    document.querySelectorAll('.test').forEach(function(item) {
        item.style.display = 'none';
    });
    results_title.style.display = '';
    results.style.display = '';
    btn_res.style.display = 'none';
}
